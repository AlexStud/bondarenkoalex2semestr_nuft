#include<windows.h>
#include <graphics.h>
#include <math.h>
#include <stdlib.h>
#include <iostream>
#include <conio.h>
float F1(float z)
{
	return 1 / (2 + pow(sin(z), 2));
}
int main()
{
	int g = DETECT, r, k, a = 0, n = 200, xg, yg, i;
	float b = 3.14, x, dx, Mx = 170, M1 = 100, M2 = 200;
	dx = fabs((b - a) / (n - 1));
	initwindow(800, 800);
	setgraphmode(1);
	setbkcolor(0);
	setcolor(15);
	outtextxy(305, 245, "0");
	outtextxy(630, 245, "X");
	outtextxy(315, 10, "Y");
	moveto(0, 240);
	linerel(640, 0);
	line(640, 240, 625, 235);
	line(640, 240, 625, 245);
	line(300, 0, 300, 670);
	line(300, 0, 295, 20);
	line(300, 0, 305, 20);
	setcolor(3);
	outtextxy(315, 320, "-2");
	outtextxy(315, 265, "-1");
	outtextxy(315, 190, "1");
	outtextxy(315, 125, "2");
	outtextxy(315, 60, "3");
	outtextxy(315, 385, "-3");
	outtextxy(315, 450, "-4");
	outtextxy(315, 515, "-5");
	outtextxy(315, 580, "-6");
	outtextxy(315, 645, "-7");
	outtextxy(320, 245, "1");
	outtextxy(385, 245, "2");
	outtextxy(450, 245, "3");
	outtextxy(515, 245, "4");
	outtextxy(580, 245, "5");
	outtextxy(255, 245, "-1");
	outtextxy(190, 245, "-2");
	outtextxy(125, 245, "-3");
	outtextxy(60, 245, "-4");
	setcolor(15);
	outtextxy(30, 30, "��������� ����-��� ������");
	for (i = 1; i < 11; i++)
	{
		line(295, i * 65, 305, i * 65);
	}
	for (i = 1; i < 10; i++)
		line(i * 65, 235, i * 65, 245);
	getch();
	x = a;
	setcolor(5);
	moveto(50, floor(M1 * F1(0)) + 185);
	for (i = 1; i <= n; i++, x += dx)
	{
		xg = floor(Mx * x);
		yg = floor(M1 * F1(x));
		lineto(xg + 50, yg + 185);
	}
	settextstyle(0, 10, 1);
	outtextxy(520, 205, "F1=1/(2+pow(sin(x),2)");
	setcolor(9);
	moveto(30, 400);
	//bar(300,500,230,450);
	rectangle(70, 300, 230, 550);
	settextstyle(0, 13, 2);
	outtextxy(125, 330, "X");
	outtextxy(180, 330, "Y");
	line(150, 300, 150, 550);
	line(70, 350, 230, 350);
	outtextxy(125, 380, "0");
	outtextxy(210, 380, "0.5");
	line(70, 400, 230, 400);
	outtextxy(125, 430, "2");
	outtextxy(210, 430, "0.4");
	line(70, 450, 230, 450);
	outtextxy(125, 480, "PI");
	outtextxy(210, 480, "0.5");
	line(70, 500, 230, 500);
	outtextxy(125, 530, "-2");
	outtextxy(210, 530, "0.3");
	getch();

	closegraph();
	return 0;
}