#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <Windows.h>
#include <stdio.h>
#include <math.h>

using namespace std;

int main()
{
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	system("cls");
	srand(time(NULL));
	const int n=5,m=5;
	int i,j,dob=1,k=0;
	int *mas = new int[n*m];
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			mas[i*m+j]=rand()%20-10;
			printf("%3d",mas[i*m+j]);
		}
		cout << endl;
	}
	for(j=0;j<m;j++)
	{
		dob=1;
		k=0;
		for(i=0;i<n;i++)
		{
			if(mas[i*m+j]<0)
			{
				dob*=mas[i*m+j];
				k++;
			}
		}
		cout << "Добуток " << j+1 << " столбика = " << dob << "    Количество = " << k << endl;
	}
	cout << endl;
	delete [] mas;
	system("Pause");
	return 0;
}

