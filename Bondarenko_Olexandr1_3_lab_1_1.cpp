#include <iostream>
#include <Windows.h>
#include <cmath>
using namespace std;
double lab1_1(double x);
int main()
{
    system("cls");
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    const int n = 11;
    double A[n];
    cout << " Сформований масив A[" << n << "] = ";
    for (int i = 0; i < n; i++)
    {
        A[i] = lab1_1(i + 2.1) / (lab1_1((i + 1) / 2) + 7.3);
        /*обчислюємо елементи массиву */ 
        cout << fixed;
        cout.precision(2); /*позначаємо, що буде в числі після коми 2 знака*/
        cout << A[i] << " "; /*виводим масив */
    }
    cout << "\n";
    system("pause");
    return 0;
}
double lab1_1(double x)
{
    return pow(cos(x), 9) - exp(sin(x));/*обчислюемо косинус с експонентою*/
}

