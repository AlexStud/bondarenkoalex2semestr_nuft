#include <conio.h>
#include <time.h>
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <windows.h>

using namespace std;

void gotoxy(int xp, int yp)
{
	COORD new_xy;
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	new_xy.X = xp; new_xy.Y = yp;
	SetConsoleCursorPosition(hStdOut,new_xy);
}

//������� ��������� ����� �� ����� �������
int p1()
{
	system("cls");
	int i, j, n=0, m=0;
	float x;
	FILE *f; //f - ������� ����
	char sf[40];// ����� ��� ������� ����� �����
	cin.ignore(1,'\n');
	printf("������ ��'� ������� ���������� �����: ");
	gets(sf);
	f=fopen(sf, " w+"); //��������� ���� ��� ���������� � ����������
	if(f == NULL)
	{
		printf("������� ������� ��� ��������� ����� \n");
		while (!kbhit());
		return 0;
	}
	printf("������ ������� ����� �������: ");
	cin>>n;
	printf("������ ������� ��������� �������: ");
	cin>>m;
	fprintf(f, "n=%d ", n); // ������� �� ������ ��� �����, �� ����������
	fprintf(f, "m=%d \n", m);
	for(i=0; i<n; i++)
	{
		for(j=0; j<(m-1); j++)
		{
			x=(rand()%100-50.0)/5;
			printf("%7.2f ", x);
			fprintf(f, "%7.2f ", x);
		}
		x=(rand()%100-50.0)/5;
		printf("%7.2f \n", x);
		fprintf(f, "%7.2f \n", x);
	}
	fclose(f);
	printf("�������� ���� %s \n",sf);
	while (!kbhit());
	return 0;
}

//��������� ��������� �� ����� � ���� ����������� �����
int p2()
{
	system("cls");
	int i, m, k=0;
	FILE *f; // ��������� ������� �����
	char sf[40]; // ����� ��� ������� ����� �����
	char str[77];
	cin.ignore(1,'\n');
	printf("������ ��'� ������� ���������� �����: ");
	gets(sf);
	f=fopen(sf, "w+");//��������� ���� ��� ���������� � ����������
	if(f == NULL)
	{
		printf("������� ������� ��� ��������� ����� \n");
		while (!kbhit());
		return 0;
	}
	printf("��� ���������� ������ �������� �����\n");
	do
	{
		printf("%d: ", k);
		gets(str);
		if (!*str) break;
		m=strlen(str);
		k++;
		for(i=0;i<m;i++)
			putc(str[i],f);
		putc('\0',f);
		putc('\n',f);
	}
	while(*str);
	printf("\n");
	fclose(f); // ��������� ����
	printf("�������� ���� %s \n",sf);
	while (!kbhit());
	return 0;
}

//��������� ��������� �� ������ � ���� ������� �� ���������
int p3()
{
	system("cls");
	int i, j, n=0, m=0;
	float x;
	FILE *f; //f - ������� ����
	char sf[40];// ����� ��� ������� ����� �����
	cin.ignore(1,'\n');
	printf("������ ��'� �������� �����: ");
	gets(sf);
	f=fopen(sf, " w+b"); //��������� ���� ��� ������ � �������� ������
	if(f == NULL)
	{
		printf("������� ������� ��� ��������� ����� \n");
		while (!kbhit());
		return 0;
	}
	printf("������ ������� ����� �������: ");
	cin>>n;
	printf("������ ������� ��������� �������: ");
	cin>>m;
	fwrite(&n, sizeof(int), 1, f); // �������� ������ ��������� �������
	fwrite(&m, sizeof(int), 1, f); // �������� ������ ��������� �������
	for(i=0; i<n; i++)
	{
		for(j=0; j<m; j++)
		{
			x=(rand()%100-50.0)/5;
			printf("%7.2f ", x);
			fwrite(&x, sizeof(float), 1, f);
		}
		cout<<endl;
	}
	fclose(f);
	printf("�������� ���� %s \n",sf);
	while (!kbhit());
	return 0;
}

//��������� ��������� �� ������ � ���� ������� �� ������
int p4()
{
	system("cls");
	int i, j, n=0, m=0;
	float x;
	FILE *f; //f - ������� ����
	char sf[40];// ����� ��� ������� ����� �����
	cin.ignore(1,'\n');
	printf("������ ��'� �������� �����: ");
	gets(sf);
	f=fopen(sf, " w+b"); //��������� ���� ��� ������ � �������� ������
	if(f == NULL)
	{
		printf("������� ������� ��� ��������� ����� \n");
		while (!kbhit());
		return 0;
	}
	printf("������ ������� ����� �������: ");
	cin>>n;
	printf("������ ������� ��������� �������: ");
	cin>>m;
	float v[m];
	fwrite(&n, sizeof(int), 1, f); // �������� ������ ��������� �������
	fwrite(&m, sizeof(int), 1, f); // �������� ������ ��������� �������
	for(i=0; i<n; i++)
	{
		for(j=0; j<m; j++)
		{
			x=(rand()%100-50.0)/5;
				v[j]=x;
				printf("%7.2f ", x);
			}
		fwrite(&v, sizeof(v), 1, f);
		cout<<endl;
	}
	fclose(f);
	printf("�������� ���� %s \n",sf);
	while (!kbhit());
	return 0;
}

// ������� 1
int p5()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j, n=0, m=0;
	float x;
	FILE *f1, *f2; //f1 - ������� ����, *f2 - �������� ����
	char sf1[40], sf2[40];// ����� ��� ������� ����� �����
	cin.ignore(1,'\n');
	printf("������ ��'� �������� �����: ");
	gets(sf1);
	f1=fopen(sf1, "r+");//��������� ���� ��� ���������� � ���������� �����
	if(f1 == NULL)
	{
		printf("������� ������� ��� ������� ����� \n");
		return 0;
	}
	fscanf(f1, "n=%d ", &n); // ������� ������� ����� �������
	fscanf(f1, "m=%d ", &m); // ������� ������� ��������� �������
	float a[n][m];
	float r[m];
	float sum=1.0;
	int k;
	for(i=0;i<n;i++)
		for(j=0;j<m;j++)
		{
			fscanf(f1, "%f ", &x); //������� �������� �������
			a[i][j]=x;
		}
	for (j = 0; j < m; j++)
	{
		sum=1.0;
		k=0;
		for (i = 0; i < n; i++)
		{
			if (a[i][j]<0)
			{
				sum *= a[i][j];
				k++;
			}
		}
		if (0<k)
			r[j]=sum/k;
		else
			r[j]=0;
	}
	
	// ��������� �� ����� ������� �� ���������� ����������
	printf("������� a[%d,%d]:\n",n,m);
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%7.2f ",a[i][j]);
		printf("\n");
	}
	printf("\n ����� r(%d): \n",m);
	for (j = 0; j < m; j++)
		printf("%7.2f ",r[j]);
	printf("\n");
	cin.ignore(1,'\n');
	printf("\n������ ��'� ����� ��� ����������: ");
	gets(sf2);
	f2=freopen(sf2, "w+", stdout); //��������� �� ��������������� ���� ��������� � ������� ����
	//���� ������� ������� �� ������������� ���� ���������, �� ��� ��������� ���� ������������ � ������� ���� � �� ���������� �� �����
	if(f2== NULL)
	{
		printf("������� ������� ��� ������� ����� \n");
		return 0;
	}
	printf("������� a[%d,%d]:\n",n,m);
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%7.2f ",a[i][j]);
		printf("\n");
	}
	printf("\n ����� r(%d): \n",m);
	for (j = 0; j < m; j++)
		printf("%7.2f ",r[j]);
	printf("\n");
	fclose(f2);
	fclose(f1);
	return 0;
}

//������� 2
int p6()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j, n=0, m=0;
	float x;
	FILE *f1, *f2; //f1 - ������� ����, *f2 - �������� ����
	char sf1[40], sf2[40];// ����� ��� ������� ����� �����
	cin.ignore(1,'\n');
	printf("������ ��'� �������� �����: ");
	gets(sf1);
	f1=fopen(sf1, "rb");//��������� ���� ��� ���������� � ���������� �����
	if(f1 == NULL)
	{
		printf("������� ������� ��� ������� ����� \n");
		return 0;
	}
	fread(&n,sizeof(int),1,f1); // ������� ������� ����� �������
	fread(&m,sizeof(int),1,f1); // ������� ������� ��������� �������
	float a[n][m];
	int v=n+m;
	float r[v];
	for(i=0;i<n;i++)
		for(j=0;j<m;j++)
			fread(&a[i][j],sizeof(float),1,f1); //������� �������� �������
	
	// ��������� �� ����� ������� �� ���������� ����������
	printf("������� a[%d,%d]:\n",n,m);
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%7.2f ",a[i][j]);
		printf("\n");
	}
	int kol=0;
	for(j=0;j<m;j++)
	{
		for(i=j+1;i<n;i++)
		{
			r[kol]=a[i][j];
			kol++;
		}
	}
	for(i=1;i<v;i++)
	{
		for(j=0;j<v-i;j++)
		{
			if(r[j]<r[j+1])
				swap(r[j],r[j+1]);
		}
	}
	
	kol=0;
	for(j=0;j<m;j++)
	{
		for(i=j+1;i<n;i++)
		{
			a[i][j]=r[kol];
			kol++;
		}
	}
	printf("������� a[%d,%d]:\n",n,m);
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%7.2f ",a[i][j]);
		printf("\n");
	}
	cin.ignore(1,'\n');
	printf("\n������ ��'� ����� ��� ����������: ");
	gets(sf2);
	f2=freopen(sf2, "w+", stdout); //��������� �� ��������������� ���� ��������� � ������� ����
	//���� ������� ������� �� ������������� ���� ���������, �� ��� ��������� ���� ������������ � ������� ���� � �� ���������� �� �����
	if(f2== NULL)
	{
		printf("������� ������� ��� ������� ����� \n");
		return 0;
	}
	printf("������� a[%d,%d]:\n",n,m);
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%7.2f",a[i][j]);
		printf("\n");
	}
	fclose(f2);
	fclose(f1);
	return 0;
}

//������� 3
int p7()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	int i, j, n=0, m=0;
	float x;
	FILE *f1, *f2; //f1 - ������� ����, *f2 - �������� ����
	char sf1[40], sf2[40];// ����� ��� ������� ����� �����
	cin.ignore(1,'\n');
	printf("������ ��'� �������� �����: ");
	gets(sf1);
	f1=fopen(sf1, "rb");//��������� ���� ��� ���������� � ���������� �����
	if(f1 == NULL)
	{
		printf("������� ������� ��� ������� ����� \n");
		return 0;
	}
	fread(&n,sizeof(int),1,f1); // ������� ������� ����� �������
	fread(&m,sizeof(int),1,f1); // ������� ������� ��������� �������
	float a[n][m];
	int v=n+m;
	float r[v];
	for(i=0;i<n;i++)
		for(j=0;j<m;j++)
			fread(&a[i][j],sizeof(float),1,f1); //������� �������� �������
	
	// ��������� �� ����� ������� �� ���������� ����������
	printf("������� a[%d,%d]:\n",n,m);
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%7.2f ",a[i][j]);
		printf("\n");
	}
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
		{
			if(j<=i&&j<=m-i-1)
				a[i][j]=0;
		}
		cout << endl;
	}	
	printf("������� a[%d,%d]:\n",n,m);
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%7.2f ",a[i][j]);
		printf("\n");
	}
	cin.ignore(1,'\n');
	printf("\n������ ��'� ����� ��� ����������: ");
	gets(sf2);
	f2=freopen(sf2, "w+", stdout); //��������� �� ��������������� ���� ��������� � ������� ����
	//���� ������� ������� �� ������������� ���� ���������, �� ��� ��������� ���� ������������ � ������� ���� � �� ���������� �� �����
	if(f2== NULL)
	{
		printf("������� ������� ��� ������� ����� \n");
		return 0;
	}
	printf("������� a[%d,%d]:\n",n,m);
	for(i=0;i<n;i++)
	{
		for(j=0;j<m;j++)
			printf("%7.2f",a[i][j]);
		printf("\n");
	}
	fclose(f2);
	fclose(f1);
	return 0;

}


int p8()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	FILE *f1, *f2; // ��������� ������� �����
	char sf1[40], sf2[40]; // ����� ��� ������� ����� �����
	char str[80]; // ����� ��� ���������� ����������� ����� � �����
	int i;
	cin.ignore(1,'\n');
	printf("������ ��'� �������� �����: ");
	gets(sf1);
	f1=fopen(sf1, "r+");//��������� ���� ��� ���������� � ���������� �����
	if(f1 == NULL)
	{
		printf("������� ������� ��� ������� ����� \n");
		return 0;
	}
	printf("������ ��'� ����� ��� ���������� : ");
	gets(sf2);
	f2=freopen(sf2, "w+", stdout); //��������� �� ��������������� ���� ��������� � ������� ����
	if(f2 == NULL)
	{
		printf("������� ������� ��� ������� ����� \n");
		return 0;
	}
	string s1,s2; // ����� ��� ����������� �����
	while (!feof(f1)) //���������� ���� ���������� �� ��� ��, ���� �������� ���� �����
	{
		fgets(str, 79, f1);// ����� � ����� s ����� ���������� ����� ��� �������� 78 ������� � �����, �� ���'������ � �������� ���������� f1
		s1=str;
		if (ferror(f1))
			printf("������� ������� ����������!"); // ���� ��������������, ��� ferror(f) ���� ���������� true
		for(i=0;i<s1.length();i++)
		{
			if(s1[i]==' ' || s1[i]=='.')
			{
				if(s2.length()==5)
				{
					cout << s2 << ", ";
				}
				s2="";
			}
			else
			{
				s2+=s1[i];
			}
		}
	}
	
	fclose(f2);
	fclose(f1);
	return 0;
}

int main()
{
	system("cls");
	SetConsoleCP(1251);
	SetConsoleOutputCP(1251);
	srand(time(NULL));
	int p;
	do
	{
		system("cls");
		gotoxy(10,5); cout << " ������� ���� ";
		gotoxy(10,7); cout << " 1. ��������� �� ����� � ������ ��������� ���� �������";
		gotoxy(10,8); cout << " 2. ��������� �� ����� � ������ ��������� ���� ���������� ����� ";
		gotoxy(10,9); cout << " 3. ��������� �� ����� � ������ ������� ���� �������";
		gotoxy(10,10); cout << " 4. ��������� �� ����� � ������ ������� ���� �������";
		gotoxy(10,11); cout << " -----------------------------������ � �������----------------------------------";
		gotoxy(10,12); cout << " 5. �������� �1";
		gotoxy(10,13); cout << " 6. �������� �2";
		gotoxy(10,14); cout << " 7. �������� �3";
		gotoxy(10,15); cout << " 8. �������� �4";
		gotoxy(10,17); cout << " 0. �����";
		gotoxy(10,18); cout << " ������ ����� ������ : ";
		cin >>p;
		switch (p)
		{
			case 1:
			{
				p1();
				break;
			}
			case 2:
			{
				p2();
				break;
			}
			case 3:
			{
				p3();
				break;
			}
			case 4:
			{
				p4();
				break;
			}
			case 5:
			{
				p5();
				break;		
			}
			case 6:
			{
				p6();
				break;
			}
			case 7:
			{
				p7();
				break;					
			}
			case 8:
			{
				p8();
				break;
			}
			
		}
	}
	while (p != 0);
	return 0;
}
